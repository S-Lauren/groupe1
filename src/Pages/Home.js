import React, { useEffect, useState } from "react";


export default function Home() {
    const [persons, setPersons] = useState([]);
    const [selected, setSelected] = useState(null);

    useEffect(() => {
        fetch('http://localhost:4000/person')
            .then(response => response.json())
            .then(data => setPersons(data));
    }, []);

    
    return (
        <section>
            <h1>Persons List</h1>
            {JSON.stringify(selected)}
            <div className="list-group">
                {persons.map(person => 
                    <button key={person.id} onClick={() => setSelected(person)} className="list-group-item">{person.name} {person.firstname}</button>
                )}
            </div>
            
            {selected && 
            <div className="modal" tabIndex="-1" role="dialog" style={{display:'block'}}>
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title">{selected.name}</h5>
                        <button type="button" className="close" onClick={() => setSelected(null)} >
                            <span>&times;</span>
                        </button>
                    </div>
                    <div className="modal-body">
                        <p>Firstname : {selected.firstname}</p>
                        <p>Age : {selected.age}</p>
                    </div>
                    </div>
                </div>
            </div>
            }
        </section>
    )
}